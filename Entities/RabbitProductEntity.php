<?php

require_once "ProductEntity.php";

class RabbitProductEntity extends ProductEntity
{

    private $type = null;
    private $data = null;

    public function __construct($name = null, $itemNumber = null, $price = null, $brand = null)
    {
        parent . __construct($name, $itemNumber, $price, $brand);
    }

    public function __get($name)
    {
        if ($name == "data") {
            return $this->data;
        }
        if ($name == "type") {
            return $this->type;
        }
        return parent . __get($name);
    }

    public function __set($name, $value)
    {
        if ($name == "data") {
            $this->data = $value;
        }
        if ($name == "type") {
            $this->type = $value;
        }
        parent . __set($name, $value);
    }

    public function __toString()
    {
        $str = parent . __toString();
        $str .= " Type: " . $this->type . " Data: " . $this->data;
        return $str;
    }

}

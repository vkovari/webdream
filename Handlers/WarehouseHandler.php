<?php

namespace Handlers;

class WarehouseHandler
{

    private $warehouses = null;

    public function __construct()
    {
        $this->warehouses = [];
    }

    public function __set($name, $value)
    {
        if ($name == "add") {
            $this->addWarehouse($value);
        }
        if ($name == "remove") {
            $this->removeWareHouse($value);
        }
        if ($name == "product") {
            $this->addProduct($value);
        }
        if ($name == "removeproduct") {
            $this->removeProduct($value);
        }
    }

    public function __get($name)
    {
        if ($name == "list") {
            return $this->warehouses;
        }
    }

    public function __toString()
    {
        $warehouses = "LIST WAREHOUSES: \n";
        foreach ($this->warehouses as $key => $value) {
            $warehouses .= $key . " " . $value->__toString() . "\n";
        }
        return $warehouses;
    }

    //
    private function addWarehouse($warehouse)
    {
        if ($this->hasWarehouse($warehouse)) {
            throw new \Exception("Exists warehouse");

        }
        array_push($this->warehouses, $warehouse);
    }

    private function removeWareHouse($warehouse)
    {
        if (!$this->hasWarehouse($warehouse)) {
            throw new \Exception("Not exists warehouse");
        }
        $index = array_search($warehouse, $this->warehouses);
        unset($this->warehouses[$index]);
    }

    private function addProduct($product)
    {
        $error = null;
        foreach ($this->warehouses as $value) {
            try {
                $value->add = $product;
                $error = null;
                return;
            } catch (\Exception $exception) {
                $error = $exception;
                continue;
            }
        }
        throw $error;
    }
    private function removeProduct($product)
    {
        $error = null;
        foreach ($this->warehouses as $value) {
            try {
                $value->remove = $product;
                return;
            } catch (\Exception $exception) {
                $error = $exception;
                continue;
            }
        }
        throw $error;
    }

    private function hasWarehouse($warehouse)
    {
        return \in_array($warehouse, $this->warehouses, true);
    }

}

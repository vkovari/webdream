<?php

namespace Entities;

class BrandEntity
{

    private $name = null;
    private $quality = null;

    public function __construct($name = null, $quality = null)
    {
        if ($name == null || $quality == null) {
            throw new \Exception("Missing parameters: name, quality must required");
        }
        if ((int) $quality < 1 || (int) $quality > 5) {
            throw new \Exception("qualityCategory must between 1-5");
        }

        $this->name = $name;
        $this->quality = $quality;

    }

    public function __toString()
    {
        return "Brand: " . $this->name . " - " . "Quality: " . $this->quality;
    }

}

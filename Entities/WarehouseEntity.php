<?php

namespace Entities;

class WarehauseEntity
{

    private $name;
    private $address;
    private $capacity;

    private $products = null;

    public function __construct($name = null, $address = null, $capacity = null)
    {
        if ($name == null || $address == null || $capacity == null) {
            throw new \Exception("Missing parameters: name, address, capacity must required");
        }
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
        //
        $this->products = [];
    }

    //PRODUCTS HANDLERS
    private function addProduct($product)
    {
        if ($this->hasProduct($product)) {
            throw new \Exception("Exists product");
        }
        if ($this->capacity - 1 < 0) {
            throw new \Exception("Not enough capacity");
        }
        array_push($this->products, $product);
        $this->capacity--;
    }

    private function removeProduct($product)
    {
        if (!$this->hasProduct($product)) {
            throw new \Exception("Not exists product");
        }
        $index = array_search($product, $this->products);
        unset($this->products[$index]);
        $this->capacity++;

    }
    //
    private function hasProduct($product)
    {
        return \in_array($product, $this->products, true);
    }

    //MAGIC METHODS ....
    public function __set($name, $value)
    {
        if ($name == "add") {
            $this->addProduct($value);
        }
        if ($name == "remove") {
            $this->removeProduct($value);
        }
    }
    public function __get($name)
    {
        if ($name == "products") {
            return $this->products;
        }
        if ($name == "name") {
            return $this->name;
        }
        if ($name == "address") {
            return $this->address;
        }
        if ($name == "capacity") {
            return $this->capacity;
        }
    }

    public function __toString()
    {
        return "Warehosue: " . $this->name . " - " . "Products: " . count($this->products) . "pc." . " - " . "Capacity: " . $this->capacity;
    }

}

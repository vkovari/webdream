<?php

namespace Entities;

class ProductEntity
{

    protected $name = null;
    protected $itemNumber = null;
    protected $price = null;
    protected $brand = null;

    public function __construct($name = null, $itemNumber = null, $price = null, $brand = null)
    {
        if ($name == null || $itemNumber == null || $price == null || $brand == null) {
            throw new \Exception("Missing parameters: name, itemNumber, price, brand must required");
        }
        $this->name = $name;
        $this->itemNumber = $itemNumber;
        $this->price = $price;
        $this->brand = $brand;
    }

    //MAGIC METHODS ....
    public function __set($name, $value)
    {
        if ($name == "name") {
            $this->name = $value;
        }
        if ($name == "itemNumber") {
            $this->itemNumber = $value;
        }
        if ($name == "price") {
            $this->price = $value;
        }
        if ($name == "brand") {
            $this->brand = $value;
        }

    }
    public function __get($name)
    {
        if ($name == "name") {
            return $this->name;
        }
        if ($name == "itemNumber") {
            return $this->itemNumber;
        }
        if ($name == "price") {
            return $this->price;
        }
        if ($name == "brand") {
            return $this->brand;
        }
    }

    public function __toString()
    {
        return "Product: " . $this->name;
    }

}

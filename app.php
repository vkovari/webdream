<?php

require_once "Entities/BrandEntity.php";
require_once "Entities/WarehouseEntity.php";
require_once "Entities/ProductEntity.php";
require_once "Handlers/WarehouseHandler.php";
require_once "TestData/WarehouseData.php";

class App
{

    private $data = null;

    private $warehouseHandler = null;

    public function __construct()
    {
        $this->data = new \TestData\WarehouseData();
        $this->warehouseHandler = new \Handlers\WarehouseHandler();
        echo "WELCOME TO WEB DREAM`s TEST APPLICATION \n";
        echo "ADDED WAREHOUESES ... \n";
        sleep(2);
        $this->addWarehouses();
        echo $this->warehouseHandler;
        echo "ADD TEST PRODUCTS ... \n";
        sleep(2);
        $this->addProducts();
        echo $this->warehouseHandler;
        echo "REMOVE RANDOM PRODUCTS ... \n";
        sleep(2);
        $this->removeProducts();
        echo $this->warehouseHandler;
        echo "GOOD BYE! \n";
    }

    private function addWarehouses()
    {
        foreach ($this->data->warehouse as $key => $value) {
            $name = $value["name"];
            $address = $value["address"];
            $capacity = $value["capacity"];
            //
            $warehouse = new \Entities\WarehauseEntity($name, $address, $capacity);
            $this->warehouseHandler->add = $warehouse;
        }
    }

    private function addProducts()
    {
        for ($i = 0; $i < 29; $i++) {
            $product = $this->data->product;
            $this->warehouseHandler->product = $product;
        }
    }
    private function removeProducts()
    {
        $products = $this->getRandomProducts();
        foreach ($products as $value) {
            $this->warehouseHandler->removeproduct = $value;
        }
    }

    private function getRandomProducts()
    {
        $products = [];
        $warehouses = $this->warehouseHandler->list;
        foreach ($warehouses as $value) {
            $list = $value->products;
            if (count($list) > 0) {
                $random = array_rand($list);
                $product = $list[$random];
                array_push($products, $product);
            }
        }
        return $products;
    }
}
$app = new App();

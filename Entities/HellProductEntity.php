<?php

require_once "ProductEntity.php";

class HellProductEntity extends ProductEntity
{

    private $color = null;
    private $weight = null;

    public function __construct($name = null, $itemNumber = null, $price = null, $brand = null)
    {
        parent . __construct($name, $itemNumber, $price, $brand);
    }

    public function __get($name)
    {
        if ($name == "color") {
            return $this->color;
        }
        if ($name == "weight") {
            return $this->weight;
        }
        return parent . __get($name);
    }

    public function __set($name, $value)
    {
        if ($name == "color") {
            $this->color = $value;
        }
        if ($name == "weight") {
            $this->weight = $value;
        }
        return parent . __set($name, $value);
    }

    public function __toString()
    {
        $str = parent . __toString();
        $str .= " Color: " . $this->color . " Weight: " . $this->weight;
        return $str;
    }

}

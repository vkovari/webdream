<?php

namespace TestData;

class WarehouseData
{

    public $warehouse = [
        [
            "name" => "Warehose HELL",
            "address" => "666, Hell, Lucifer street 1.",
            "capacity" => 10,
        ],
        [
            "name" => "HEAVENS",
            "address" => "1, Haeven, Everything place 1.",
            "capacity" => 11,
        ],
        [
            "name" => "ASGARD",
            "address" => "878, Asgard, Thor Castle 1",
            "capacity" => 13,
        ],
        [
            "name" => "Midgard",
            "address" => "1137, Budapest, Váci út. 67",
            "capacity" => 15,
        ],
    ];

    private $brandNames = ["Nike", "Apple", "Microsoft", "Hublot", "Adidas", "Puma", "Nike"];

    public function __get($name)
    {
        if ($name == "product") {
            return $this->generateProduct();
        }
    }

    private function generateProduct()
    {
        $name = "Product " . rand(12, 100);
        $price = rand(500, 1500);
        //
        $brand = $this->generateBrand();
        $itemNumber = $this->generateItemNumber();
        $product = new \Entities\ProductEntity($name, $itemNumber, $price, $brand);
        return $product;
    }

    private function generateBrand()
    {
        $randomIdx = rand(0, count($this->brandNames) - 1);
        $name = $this->brandNames[$randomIdx];
        $quality = rand(1, 5);
        $brand = new \Entities\BrandEntity($name, $quality);
        return $brand;
    }
    private function generateItemNumber()
    {
        return uniqid();
    }

}

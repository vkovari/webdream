<?php

require_once "Entities/BrandEntity.php";
require_once "Entities/WarehouseEntity.php";
require_once "Entities/ProductEntity.php";
require_once "Handlers/WarehouseHandler.php";
require_once "TestData/WArehouseData.php";

class Simulate
{

    protected $warehouseNumber = 2;
    protected $warehouseCapacity = 5;
    protected $productsCount = 10;
    protected $removeProductsCount = 7;

    protected $data = null;
    protected $warehouseHandler = null;

    protected $products = null;

    public function __construct($productsCount = null, $removeProductsCount = null)
    {
        if ($productsCount != null) {
            $this->productsCount = $productsCount;
        }
        if ($removeProductsCount != null) {
            $this->removeProductsCount = $removeProductsCount;
        }
        $this->products = [];
        $this->data = new \TestData\WarehouseData();
        $this->warehouseHandler = new \Handlers\WarehouseHandler();

    }

    public function Simulated()
    {
        echo "ADD WAREHOUSES ... \n";
        sleep(1);
        $this->addWarehouses();
        echo $this->warehouseHandler;
        echo "ADD PRODUCTS ... \n";
        sleep(1);
        $this->addProducts();
        echo $this->warehouseHandler;
        echo "REMOVE PRODUCTS ... \n";
        sleep(1);
        $this->removeProducts();
        echo $this->warehouseHandler;
        echo "END SIMULATED ... BYE \n";
    }
    protected function addWarehouses()
    {
        foreach ($this->data->warehouse as $key => $value) {
            if ($key < $this->warehouseNumber) {
                $name = $value["name"];
                $address = $value["address"];
                //
                $warehouse = new \Entities\WarehauseEntity($name, $address, $this->warehouseCapacity);
                $this->warehouseHandler->add = $warehouse;
            }
        }
    }
    protected function addProducts()
    {
        for ($i = 0; $i < $this->productsCount; $i++) {
            $product = $this->data->product;
            try {
                $this->warehouseHandler->product = $product;
                array_push($this->products, $product);
            } catch (\Exception $exception) {
                throw $exception;
            }
        }
    }
    protected function removeProducts()
    {
        foreach ($this->products as $key => $value) {
            if ($key < $this->removeProductsCount) {
                $this->warehouseHandler->removeproduct = $value;
            }
        }
    }

}
class SimulateThree extends Simulate
{

    protected function addProducts()
    {
        for ($i = 0; $i < $this->removeProductsCount; $i++) {
            $product = $this->data->product;
            array_push($this->products, $product);
            if ($i < $this->productsCount) {
                try {
                    $this->warehouseHandler->product = $product;
                } catch (\Exception $exception) {
                    throw $exception;
                }
            }
        }
    }

}

class App
{

    public function __construct()
    {
        echo "SIMULATION ONE \n";
        $simulate = new Simulate();
        try {
            $simulate->Simulated();
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
        }
        //
        sleep(1);
        echo "==================";
        echo "SIMULATION TWO \n";

        $simulate = new Simulate(15);
        try {
            $simulate->Simulated();
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
            exit();
        }
        sleep(1);
        echo "==================";
        echo "SIMULATION TWO \n";
        $simulate = new SimulateThree(10, 13);
        try {
            $simulate->Simulated();
        } catch (\Exception $e) {
            echo $e->getMessage() . "\n";
            exit;
        }

    }
}

$app = new App();
